function createCard(name, description, pictureUrl, starts, ends, location) {
  return `
  <div class="col h-auto">
    <div class="card shadow p-0 mb-5 bg-body rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        <small class="text-muted">${starts} - ${ends}</small>
      </div>
    </div>
    </div>
  `;
}

function alert() {
  return `
  <div class="alert alert-primary" role="alert">
  A simple primary alert—check it out!
  </div>
  `
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      const row = document.querySelector('.row');
      row.innerHTML += alert(); 
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startsObj = new Date(details.conference.starts);
          const options = {year: 'numeric', month: 'short', day: 'numeric' };
          const starts = startsObj.toLocaleDateString('en-us', options);
          const endsObj = new Date(details.conference.ends);
          const ends = endsObj.toLocaleDateString('en-us', options);
          const location = details.conference.location.name;
          const html = createCard(name, description, pictureUrl, starts, ends, location);
          const row = document.querySelector('.row');
          row.innerHTML += html;          
        }
      }

    }
  } catch (e) {
    const row = document.querySelector('.row');
    row.innerHTML += alert(); 
  }

});